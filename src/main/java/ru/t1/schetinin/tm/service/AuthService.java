package ru.t1.schetinin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.api.service.IAuthService;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.api.service.IUserService;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.exception.field.LoginEmptyException;
import ru.t1.schetinin.tm.exception.field.PasswordEmptyException;
import ru.t1.schetinin.tm.exception.user.AccessDeniedException;
import ru.t1.schetinin.tm.exception.user.AuthenticationException;
import ru.t1.schetinin.tm.exception.user.PermissionException;
import ru.t1.schetinin.tm.model.User;
import ru.t1.schetinin.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(@NotNull final IPropertyService propertyService, @NotNull final IUserService userService) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

    @NotNull
    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        final boolean locked = user.getLocked() == null || user.getLocked();
        if (locked) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        @NotNull final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

}