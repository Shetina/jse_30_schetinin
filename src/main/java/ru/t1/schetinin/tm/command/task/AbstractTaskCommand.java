package ru.t1.schetinin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.api.service.IProjectTaskService;
import ru.t1.schetinin.tm.api.service.ITaskService;
import ru.t1.schetinin.tm.command.AbstractCommand;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}