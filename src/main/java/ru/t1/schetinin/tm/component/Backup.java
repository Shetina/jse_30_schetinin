package ru.t1.schetinin.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.command.data.AbstractDataCommand;
import ru.t1.schetinin.tm.command.data.DataBackupLoadCommand;
import ru.t1.schetinin.tm.command.data.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;


    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

    @SneakyThrows
    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            save();
        }
    }

}